import torch
import numpy as np

from sklearn.preprocessing import StandardScaler
from torch.utils.data import Dataset, DataLoader
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset, DataLoader

### Default global variables that are unlinkely to change
COND_INPUT_SIZE = 9
NOISE_INPUT_SIZE = 10
IMAGE_SIZE = 44

### Wasserstein loss params
BLUR = 0.00001
SCALING = 0.95
DIAMETER = 0.01
AUTOENCODER_LOSS_SCALING = 100


class ParticleDataset(Dataset):

    def __init__(self, particle_file, image_file, min_pixels=10, img_size=44):
        self.particles = np.load(particle_file)["arr_0"]
        self.images = np.load(image_file)["arr_0"]

        self.particles = torch.tensor(self.particles, dtype=torch.float)
        self.scaler = StandardScaler()
        self.particles = np.float32(self.particles)
        self.particles = self.scaler.fit_transform(self.particles)

        self.images = torch.tensor(self.images, dtype=torch.float)
        valid_images = ((self.images > 0).sum(axis=1).sum(axis=1) > min_pixels).squeeze()
        self.particles = self.particles[valid_images]
        self.images = self.images[valid_images]
        self.images = self.images.reshape([-1, 1, img_size, img_size])
        self.images = np.log(self.images + 1)

    def __len__(self):
        return len(self.particles)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        sample_p = self.particles[idx]
        sample_i = self.images[idx]
        sample = sample_i, sample_p
        # self.transform(sample_i)

        return sample


in_images_channels = 1
image_size = 44


class Autoencoder(nn.Module):
    def __init__(self, g_input_dim, cond_dim):
        super(Autoencoder, self).__init__()
        self.latent_size = 20
        self.d = 128
        self.conv1 = nn.Conv2d(in_channels=in_images_channels, out_channels=self.d, kernel_size=4, stride=2, padding=1)
        self.cbn1 = nn.BatchNorm2d(self.d)
        self.conv2 = nn.Conv2d(self.d, self.d * 2, kernel_size=4, stride=2, padding=1)
        self.cbn2 = nn.BatchNorm2d(self.d * 2)
        self.conv3 = nn.Conv2d(self.d * 2, self.d, kernel_size=4, stride=2, padding=1)
        self.cbn3 = nn.BatchNorm2d(self.d)
        self.fc3 = nn.Linear(self.d * 25, self.latent_size)

        self.fc1 = nn.Linear(self.latent_size, self.d * 4 * 4)
        self.dc1 = nn.ConvTranspose2d(self.d, self.d * 2, 4, 2, 0, bias=False)
        self.dc1_bn = nn.BatchNorm2d(self.d * 2)
        self.dc2 = nn.ConvTranspose2d(self.d * 2, self.d * 2, 4, 2, 4, bias=False)
        self.dc2_bn = nn.BatchNorm2d(self.d * 2)
        self.dc3 = nn.ConvTranspose2d(self.d * 2, self.d, 4, 2, 3, bias=False)
        self.dc3_bn = nn.BatchNorm2d(self.d)
        self.dc4 = nn.ConvTranspose2d(self.d, in_images_channels, 4, 2, 3, bias=False)

        ############### ng

        self.ng_fc1 = nn.Linear(g_input_dim, self.d * 2)
        self.ng_input_2 = nn.Linear(cond_dim, self.d)
        self.ng_fc2 = nn.Linear(self.d * 3, self.d * 4)
        self.ng_fc3 = nn.Linear(self.d * 4, self.d * 4)
        self.ng_fc4 = nn.Linear(self.d * 4, self.latent_size)

        for m in self.modules():
            classname = m.__class__.__name__
            if classname.find('Conv') != -1:
                nn.init.normal_(m.weight.data, 0.0, 0.02)
            elif (classname.find('BatchNorm') != -1):  # |(classname.find('Linear') != -1):
                nn.init.normal_(m.weight.data, 1.0, 0.02)
                nn.init.constant_(m.bias.data, 0)

    # forward method
    def forward(self, x):
        x = self.conv1(x)
        x = F.leaky_relu(self.cbn1(x))
        x = self.conv2(x)
        x = F.leaky_relu(self.cbn2(x))
        x = self.conv3(x)
        x = F.leaky_relu(self.cbn3(x))
        x = x.view([-1, self.d * 25])
        y = F.leaky_relu(self.fc3(x))

        x = F.leaky_relu(self.fc1(y))
        x = x.view(-1, self.d, 4, 4)
        x = self.dc1(x)
        x = F.leaky_relu(self.dc1_bn(x))
        x = self.dc2(x)
        x = F.leaky_relu(self.dc2_bn(x))
        x = self.dc3(x)
        x = F.leaky_relu(self.dc3_bn(x))
        return torch.relu(self.dc4(x)), y

    def generate(self, x):
        x = F.leaky_relu(self.fc1(x))
        x = x.view(-1, self.d, 4, 4)
        x = self.dc1(x)
        x = F.leaky_relu(self.dc1_bn(x))
        x = self.dc2(x)
        x = F.leaky_relu(self.dc2_bn(x))
        x = self.dc3(x)
        x = F.leaky_relu(self.dc3_bn(x))
        return torch.relu(self.dc4(x))

    def generate_noise(self, x, cond_x):
        x = F.leaky_relu(self.ng_fc1(x), 0.2)
        x2 = F.leaky_relu(self.ng_input_2(cond_x), 0.2)
        x_concat = torch.cat((x, x2), 1)
        x = F.leaky_relu(self.ng_fc2(x_concat), 0.2)
        x = F.leaky_relu(self.ng_fc3(x), 0.2)
        return F.leaky_relu(self.ng_fc4(x))

    def full_generate(self, x, cond_x):
        noise = self.generate_noise(x, cond_x)
        out = self.generate(noise)
        return out
