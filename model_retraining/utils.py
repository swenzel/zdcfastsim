import numpy as np

def sum_channels_parallel_wrong(data):
    img_channel = np.array([])
    img_channel = np.append(img_channel,\
                            data[:,:22,:22].sum(axis=(1,2))\
                            -(data[:,:22,:22].sum(axis=1).reshape(-1,11,2).sum(axis=1)[:,1]\
                              + data[:,:22,:22].sum(axis=2).reshape(-1,11,2).sum(axis=1)[:,1]\
                              - data[:,:22,:22].reshape(-1,11,22*2).sum(axis=1)[:,22:].reshape(-1,11,2).sum(axis=1)[:,1])
                           )
    img_channel = np.append(img_channel,\
                            data[:,22:,:22].sum(axis=(1,2))\
                            -(data[:,22:,:22].sum(axis=1).reshape(-1,11,2).sum(axis=1)[:,1]\
                              + data[:,22:,:22].sum(axis=2).reshape(-1,11,2).sum(axis=1)[:,1]\
                              - data[:,22:,:22].reshape(-1,11,22*2).sum(axis=1)[:,22:].reshape(-1,11,2).sum(axis=1)[:,1])
                           )
    img_channel = np.append(img_channel,\
                            data[:,:22,22:].sum(axis=(1,2))\
                            -(data[:,:22,22:].sum(axis=1).reshape(-1,11,2).sum(axis=1)[:,1]\
                              + data[:,:22,22:].sum(axis=2).reshape(-1,11,2).sum(axis=1)[:,1]\
                              - data[:,:22,22:].reshape(-1,11,22*2).sum(axis=1)[:,22:].reshape(-1,11,2).sum(axis=1)[:,1])
                           )
    img_channel = np.append(img_channel,\
                            data[:,22:,22:].sum(axis=(1,2))\
                            -(data[:,22:,22:].sum(axis=1).reshape(-1,11,2).sum(axis=1)[:,1]\
                              + data[:,22:,22:].sum(axis=2).reshape(-1,11,2).sum(axis=1)[:,1]\
                              - data[:,22:,22:].reshape(-1,11,22*2).sum(axis=1)[:,22:].reshape(-1,11,2).sum(axis=1)[:,1])
                           )
    img_channel = np.append(img_channel,\
                            data.sum(axis=1).reshape(-1,22,2).sum(axis=1)[:,1]\
                            + data.sum(axis=2).reshape(-1,22,2).sum(axis=1)[:,1]\
                            - data.reshape(-1,22,44*2).sum(axis=1)[:,44:].reshape(-1,22,2).sum(axis=1)[:,1]
                           )
    return img_channel.reshape(5,-1).T

def sum_channels_parallel_(data):
    
   
    
    coords = np.ogrid[0:data.shape[1], 0:data.shape[2]]
    half_x = data.shape[1]//2
    half_y = data.shape[2]//2

    checkerboard = (coords[0] + coords[1]) % 2 != 0
    checkerboard.reshape(-1,checkerboard.shape[0], checkerboard.shape[1])
    
    ch5 = (data*checkerboard).sum(axis=1).sum(axis=1)
    
    checkerboard = (coords[0] + coords[1]) % 2 == 0
    checkerboard =checkerboard.reshape(-1,checkerboard.shape[0], checkerboard.shape[1])
    
    mask = np.zeros((1,data.shape[1],data.shape[2]))
    mask[:,:half_x,:half_y] = checkerboard[:,:half_x,:half_y]
    ch1 = (data*mask).sum(axis=1).sum(axis=1)

    mask = np.zeros((1,data.shape[1],data.shape[2]))
    mask[:,:half_x,half_y:] = checkerboard[:,:half_x,half_y:]
    ch2 = (data*mask).sum(axis=1).sum(axis=1)
    
    mask = np.zeros((1,data.shape[1],data.shape[2]))
    mask[:,half_x:,:half_y] = checkerboard[:,half_x:,:half_y]
    ch3 = (data*mask).sum(axis=1).sum(axis=1)
    
    mask = np.zeros((1,data.shape[1],data.shape[2]))
    mask[:,half_x:,half_y:] = checkerboard[:,half_x:,half_y:]
    ch4 = (data*mask).sum(axis=1).sum(axis=1)
    
    #assert all(ch1+ch2+ch3+ch4+ch5 == data.sum(axis=1).sum(axis=1))==True
    
    return zip(ch1,ch2,ch3,ch4,ch5)