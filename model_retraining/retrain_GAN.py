#!/usr/bin/env python
# coding: utf-8

# # VAE model
# ##### Script for training model

import os
import argparse

os.environ["CUDA_VISIBLE_DEVICES"] = "0"
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'


import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import StratifiedKFold, KFold, train_test_split
import time
from numpy import load
import tensorflow as tf
from tensorflow import keras

print(tf.__version__)

from tensorflow.compat.v1.keras.layers import Input, Dense, LeakyReLU, Conv2D, MaxPooling2D, UpSampling2D, Concatenate
from tensorflow.compat.v1.keras.models import Model
from tensorflow.compat.v1.keras.layers import Dense, Reshape, Flatten
from tensorflow.compat.v1.keras.layers import Dropout, BatchNormalization
from scipy.stats import wasserstein_distance
import pandas as pd
from utils import sum_channels_parallel_ as sum_channels_parallel


def main(name, epochs, data_dir):
    NAME = name
    os.makedirs("images", exist_ok=True)
    os.makedirs("models", exist_ok=True)

    # load the dataset
    data = load(os.path.join(data_dir, 'data_nonrandom_responses.npz'))["arr_0"][:1000]
    print('Loaded: ', data.shape, "max:", data.max())

    data_cond = load(os.path.join(data_dir, 'data_nonrandom_particles.npz'))["arr_0"][:1000]
    data_cond = pd.DataFrame(data_cond, columns=['Energy', 'Vx', 'Vy', 'Vz', 'Px', 'Py', 'Pz', 'mass', 'charge'])
    COND_DIM = data_cond.shape[-1]
    print('Loaded cond: ', data_cond.shape)

    # preprocess data
    data = np.log(data + 1)
    data = np.float32(data)
    print("data max", data.max(), "min", data.min())

    scaler = StandardScaler()
    data_cond = np.float32(data_cond)
    data_cond = scaler.fit_transform(data_cond)
    print("cond max", data_cond.max(), "min", data_cond.min())

    # train/test split
    x_train, x_test, y_train, y_test, = train_test_split(data, data_cond, test_size=0.2, shuffle=False)
    print(x_train.shape, x_test.shape, y_train.shape, y_test.shape)

    # make tf datasets
    dataset = tf.data.Dataset.from_tensor_slices(x_train).batch(batch_size=128)
    dataset_cond = tf.data.Dataset.from_tensor_slices(y_train).batch(batch_size=128)
    fake_cond =  tf.data.Dataset.from_tensor_slices(y_train).shuffle(12800).batch(batch_size=128)
    dataset_with_cond = tf.data.Dataset.zip((dataset,dataset_cond, fake_cond)).shuffle(12800)

    val_dataset = tf.data.Dataset.from_tensor_slices(x_test).batch(batch_size=128)
    val_dataset_cond = tf.data.Dataset.from_tensor_slices(y_test).batch(batch_size=128)
    val_fake_cond =  tf.data.Dataset.from_tensor_slices(y_test).shuffle(12800).batch(batch_size=128)
    val_dataset_with_cond = tf.data.Dataset.zip((val_dataset,val_dataset_cond,val_fake_cond)).shuffle(12800)



    ############################ Define Models ############################
    latent_dim = 10
    cond_dim = 9

    ############################ generator ############################

    x = Input(shape=(latent_dim,))
    cond = Input(shape=(cond_dim,))
    inputs = Concatenate(axis=1)([x, cond])
    layer_1 = Dense(128 * 2)(inputs)
    layer_1_bd = Dropout(0.2)(BatchNormalization()(layer_1))
    layer_1_a = LeakyReLU(alpha=0.1)(layer_1_bd)
    layer_2 = Dense(128 * 13 * 13)(layer_1_a)
    layer_2_bd = Dropout(0.2)(BatchNormalization()(layer_2))
    layer_2_a = LeakyReLU(alpha=0.1)(layer_2_bd)
    reshaped = Reshape((13, 13, 128))(layer_2_a)
    reshaped_s = UpSampling2D()(reshaped)
    conv1 = Conv2D(128, kernel_size=3)(reshaped_s)
    conv1_bd = Dropout(0.2)(BatchNormalization()(conv1))
    conv1_a = LeakyReLU(alpha=0.1)(conv1_bd)
    conv1_a_s = UpSampling2D()(conv1_a)
    conv2 = Conv2D(64, kernel_size=3)(conv1_a_s)
    conv2_bd = Dropout(0.2)(BatchNormalization()(conv2))
    conv2_a = LeakyReLU(alpha=0.1)(conv2_bd)
    outputs = Conv2D(1, kernel_size=3, activation='relu')(conv2_a)

    generator = Model([x, cond], outputs, name='generator')
    generator.summary()

    ############################ discriminator ############################

    input_img = Input(shape=[44, 44, 1], name='input_img')
    conv1 = Conv2D(32, kernel_size=3)(input_img)
    conv1_bd = Dropout(0.2)(BatchNormalization()(conv1))
    conv1_a = LeakyReLU(alpha=0.1)(conv1_bd)
    pool1 = MaxPooling2D(pool_size=(2, 2))(conv1_a)
    conv2 = Conv2D(16, kernel_size=3)(pool1)
    conv2_bd = Dropout(0.2)(BatchNormalization()(conv2))
    conv2_a = LeakyReLU(alpha=0.1)(conv2_bd)
    pool2 = MaxPooling2D(pool_size=(2, 2))(conv2_a)
    flat = Flatten()(pool2)
    cond = Input(shape=(cond_dim,))
    inputs2 = Concatenate(axis=1)([flat, cond])
    layer_1 = Dense(128)(inputs2)
    layer_1_bd = Dropout(0.2)(BatchNormalization()(layer_1))
    layer_1_a = LeakyReLU(alpha=0.1)(layer_1_bd)
    layer_2 = Dense(64)(layer_1_a)
    layer_2_bd = Dropout(0.2)(BatchNormalization()(layer_2))
    layer_2_a = LeakyReLU(alpha=0.1)(layer_2_bd)
    outputs = Dense(1, activation='sigmoid')(layer_2_a)

    discriminator = Model([input_img, cond], outputs, name='discriminator')
    discriminator.summary()

    def discriminator_loss(real_output, fake_output):
        real_loss = cross_entropy(tf.ones_like(real_output), real_output)
        fake_loss = cross_entropy(tf.zeros_like(fake_output), fake_output)
        total_loss = real_loss + fake_loss
        d_acc_r.update_state(tf.ones_like(real_output), real_output)
        d_acc_f.update_state(tf.zeros_like(fake_output), fake_output)
        return total_loss

    def generator_loss(step, fake_output):
        g_acc.update_state(tf.ones_like(fake_output), fake_output)
        return cross_entropy(tf.ones_like(fake_output), fake_output)

    # define losses
    cross_entropy = tf.keras.losses.BinaryCrossentropy(from_logits=True)
    d_acc_r = keras.metrics.BinaryAccuracy(name="d_acc_r", threshold=0.5)
    d_acc_f = keras.metrics.BinaryAccuracy(name="d_acc_r", threshold=0.5)
    g_acc = keras.metrics.BinaryAccuracy(name="g_acc_g", threshold=0.5)

    # define optimizer
    generator_optimizer = tf.keras.optimizers.Adam(1e-4)
    discriminator_optimizer = tf.keras.optimizers.Adam(1e-4)

    # trainin params

    EPOCHS = epochs
    noise_dim = 10
    num_examples_to_generate = 16

    # Seed to reuse for generating samples for comparison during training
    seed = tf.random.normal([num_examples_to_generate, noise_dim])
    seed_cond = y_test[20:20 + num_examples_to_generate]

    ### function to calculate ws distance between orginal and generated channels
    org = np.exp(x_test) - 1
    ch_org = np.array(org).reshape(-1, 44, 44)
    ch_org = pd.DataFrame(sum_channels_parallel(ch_org)).values
    del org

    def calculate_ws_ch(n_calc):
      ws= [0,0,0,0,0]
      for j in range(n_calc):
        z = np.random.normal(0,1,(x_test.shape[0],10))
        z_c = y_test
        results = generator.predict([z,z_c])
        results = np.exp(results)-1
        try:
          ch_gen = np.array(results).reshape(-1,44,44)
          ch_gen = pd.DataFrame(sum_channels_parallel(ch_gen)).values
          for i in range(5):
            ws[i] = ws[i] + wasserstein_distance(ch_org[:,i], ch_gen[:,i])
          ws =np.array(ws)
        except ValueError as e:
          print(e)
      ws = ws/n_calc
      print("ws mean",f'{ws.sum()/5:.2f}', end=" ")
      for n,score in enumerate(ws):
        print("ch"+str(n+1),f'{score:.2f}',end=" ")
      return ws.mean()
    
    ####################### training ##############################
    @tf.function
    def train_step(batch, step):
        images, cond, noise_cond = batch
        step = step
        BATCH_SIZE = tf.shape(images)[0]
        noise = tf.random.normal([BATCH_SIZE, noise_dim])

        with tf.GradientTape() as gen_tape, tf.GradientTape() as disc_tape:
            generated_images = generator([noise, noise_cond], training=True)

            real_output = discriminator([images, cond], training=True)
            fake_output = discriminator([generated_images, noise_cond], training=True)

            gen_loss = generator_loss(step, fake_output)
            disc_loss = discriminator_loss(real_output, fake_output)

        gradients_of_generator = gen_tape.gradient(gen_loss, generator.trainable_variables)
        gradients_of_discriminator = disc_tape.gradient(disc_loss, discriminator.trainable_variables)
        generator_optimizer.apply_gradients(zip(gradients_of_generator, generator.trainable_variables))
        discriminator_optimizer.apply_gradients(zip(gradients_of_discriminator, discriminator.trainable_variables))

        return gen_loss, disc_loss

    history = []

    def train(dataset, epochs):
        best_ws_distance = 10*5
        tf_step = tf.Variable(0, dtype=float)
        step = 0
        generate_and_save_images(generator,
                                 epochs,
                                 [seed, seed_cond])

        for epoch in range(epochs):
            start = time.time()

            for batch in dataset:
                gen_loss, disc_loss = train_step(batch, tf_step)
                history.append([gen_loss, disc_loss,
                                100 * d_acc_r.result().numpy(),
                                100 * d_acc_f.result().numpy(),
                                100 * g_acc.result().numpy(),
                                ])
                tf_step.assign_add(1)
                step = step + 1

                if step % 100 == 0:
                    print("%d [D real acc: %.2f%%] [D fake acc: %.2f%%] [G acc: %.2f%%] " % (
                        step,
                        100 * d_acc_r.result().numpy(),
                        100 * d_acc_f.result().numpy(),
                        100 * g_acc.result().numpy()))
                if step % 1000 == 0:
                    generate_and_save_images(generator,
                                             epochs,
                                             [seed, seed_cond])

            generate_and_save_images(generator,
                                     epoch + 1,
                                     [seed, seed_cond]
                                     )

            # Save the model
            generator.save_weights("gen_" + NAME + "_" + str(epoch) + ".h5")
            discriminator.save_weights("disc_" + NAME + "_" + str(epoch) + ".h5")
            np.savez("history_" + NAME + ".npz", np.array(history))
            
            ws_distance = calculate_ws_ch(min(epoch // 5 + 1, 5))
            if ws_distance < best_ws_distance:
                best_ws_distance = ws_distance
                generator.save_weights("gen_" + NAME + "_" + "best.h5")
                discriminator.save_weights("disc_" + NAME + "_" + "best.h5")

            print('Time for epoch {} is {} sec'.format(epoch + 1, time.time() - start))

        return history

    def generate_and_save_images(model, epoch, test_input):
        # Notice `training` is set to False.
        # This is so all layers run in inference mode (batchnorm).
        predictions = model(test_input, training=False)

        fig, axs = plt.subplots(2, 7, figsize=(15, 4))
        for i in range(0, 14):
            if i < 7:
                x = x_test[20 + i].reshape(44, 44)
            else:
                x = predictions[i - 7].numpy().reshape(44, 44)
            # x[x<=0]=x.max()*-0.1
            im = axs[i // 7, i % 7].imshow(x, interpolation='none', cmap='gnuplot')
            axs[i // 7, i % 7].axis('off')
            fig.colorbar(im, ax=axs[i // 7, i % 7])
        plt.show()
        plt.savefig('./images/image_at_epoch_{:04d}.png'.format(epoch))

    # ### Train model
    history = train(dataset_with_cond, EPOCHS)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='train VAE')
    parser.add_argument('--name', help='model name', required=True)
    parser.add_argument('--epochs', type=int, help='epochs to train', default=100)
    parser.add_argument('--data',
                        help='path to data dir with data_nonrandom_responses.npz and data_nonrandom_particles.npz',
                        default="data")
    args = parser.parse_args()

    main(args.name, args.epochs, args.data)