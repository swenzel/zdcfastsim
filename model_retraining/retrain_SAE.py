#!/usr/bin/env python
# coding: utf-8

# # VAE model
# ##### Script for training model


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import Dataset, DataLoader
from torch.utils.data.sampler import SubsetRandomSampler
from geomloss import SamplesLoss
import time
import argparse

import os

from scipy.stats import wasserstein_distance
import pandas as pd
from utils import sum_channels_parallel_ as sum_channels_parallel
from sklearn.metrics import mean_absolute_error

from model_retraining.sae_utils import ParticleDataset, Autoencoder, NOISE_INPUT_SIZE, COND_INPUT_SIZE, BLUR, SCALING, \
    DIAMETER, AUTOENCODER_LOSS_SCALING, IMAGE_SIZE


def main(args):
    save_dir = f"results/{args.name}"
    os.makedirs(f"{save_dir}/images", exist_ok=True)
    os.makedirs(f"{save_dir}/models", exist_ok=True)

    if int(args.gpu_id) >= 0:
        print(f"Using gpu: {args.gpu_id}")
        torch.cuda.set_device(int(args.gpu_id))
        device = torch.device(f"cuda")
    else:
        print("Training without gpu")
        device = torch.device("cpu")

    # load the dataset
    print("Loading data")
    data_path = os.path.join(args.data, 'data_nonrandom_responses.npz')
    data_cond_path = os.path.join(args.data, 'data_nonrandom_particles.npz')

    dataset = ParticleDataset(data_cond_path, data_path)

    # train/test split
    dataset_size = len(dataset)
    indices = list(range(dataset_size))
    np.random.shuffle(indices)
    split = int(dataset_size * (1 - float(args.testset_size)))
    train_indices, val_indices = indices[:-split], indices[-split:]

    train_sampler = SubsetRandomSampler(train_indices)
    valid_sampler = SubsetRandomSampler(val_indices)

    dataloader = torch.utils.data.DataLoader(dataset, batch_size=int(args.batch_size),
                                             drop_last=True, sampler=train_sampler)

    validation_loader = torch.utils.data.DataLoader(dataset, batch_size=int(args.batch_size),
                                                    sampler=valid_sampler)

    # Create model
    autoencoder = Autoencoder(NOISE_INPUT_SIZE, COND_INPUT_SIZE).to(device)
    autoencoder.train()

    # Define losses
    loss_func = SamplesLoss("sinkhorn", blur=BLUR, scaling=SCALING, diameter=DIAMETER, debias=True)
    criterion = nn.MSELoss()
    mse_loss = nn.MSELoss()
    a_optimizer = optim.Adam(autoencoder.parameters(), lr=float(args.lr))

    print("Start training")
    start = time.time()
    for epoch in range(1, int(args.epochs) + 1):
        ng_losses = []
        a_losses = []
        mse_losses = []
        if epoch % 30 == 29:
            for param_group in a_optimizer.param_groups:
                param_group['lr'] = param_group['lr'] / 2
                print("lr=", param_group['lr'])
        for batch_idx, (x, cond_x) in enumerate(dataloader):
            x = x.to(device)
            cond_x = cond_x.to(device)

            autoencoder.zero_grad()
            autoencoder_output, y = autoencoder(x)
            a_loss = criterion(autoencoder_output, x)

            a_losses.append(a_loss)

            rand_x = torch.randn(int(args.batch_size), NOISE_INPUT_SIZE).to(device)
            rand_y = autoencoder.generate_noise(rand_x, cond_x)
            ng_loss = loss_func(torch.cat([y, cond_x], 1), torch.cat([rand_y, cond_x], 1))
            mse_ng = mse_loss(y, rand_y)

            s_loss = ng_loss + AUTOENCODER_LOSS_SCALING * a_loss  # + 1* mse_ng
            s_loss.backward()
            a_optimizer.step()

            ng_losses.append(ng_loss)
            mse_losses.append(mse_ng)

        print('[%d/%d]: loss_ng: %.4f, loss_a: %.4f, loss_mse: %.6f' % (
            (epoch), int(args.epochs), torch.mean(torch.FloatTensor(ng_losses)),
            torch.mean(torch.FloatTensor(a_losses)),
            torch.mean(torch.FloatTensor(mse_losses))))
    print(f"Training finished after {time.strftime('%H hours %M minutes %S seconds', time.gmtime(int(time.time() - start)))}")
    autoencoder.eval()

    torch.save(autoencoder.state_dict(), f"{save_dir}/models/sae_model.th")
    dummy_input = next(iter(dataloader))[0][0:1].cpu()
    torch.onnx.export(autoencoder.cpu(), dummy_input,
                      f"{save_dir}/models/sae_model.onnx",
                      input_names=["input"], output_names=["output"])

    x, cond_x = next(iter(validation_loader))
    img_shape = [IMAGE_SIZE, IMAGE_SIZE]

    plt.figure(figsize=(25, 15))
    fig, axs = plt.subplots(3, 5)
    autoencoder.to(device)
    for i in range(5):
        example = x[i:i + 1]
        example = example.cpu().detach().numpy()
        example = example.swapaxes(1, 3).squeeze()
        axs[0,i].imshow(np.rot90(example, 3))
        test = example.reshape(img_shape)
        ch_gen = np.array(test).reshape(-1, IMAGE_SIZE, IMAGE_SIZE)
        ch_gen = pd.DataFrame(sum_channels_parallel(ch_gen)).values
        axs[0,i].set_title("Original\n" + str(ch_gen.astype(int)[0]))
        axs[0,i].axis('off')

        example, _ = autoencoder.forward(x[i:i + 1].to(device))
        example = example.cpu().detach().numpy()
        example = example.swapaxes(1, 3).squeeze()

        axs[1,i].imshow(np.rot90(example, 3))
        test = example.reshape(img_shape)
        ch_gen = np.array(test).reshape(-1, IMAGE_SIZE, IMAGE_SIZE)
        ch_gen = pd.DataFrame(sum_channels_parallel(ch_gen)).values
        axs[1,i].set_title("Decoded\n" + str(ch_gen.astype(int)[0]))
        axs[1, i].axis('off')

        generated_noise = autoencoder.generate_noise(torch.randn(1, NOISE_INPUT_SIZE).to(device), cond_x[i:i + 1].to(device))
        example = autoencoder.generate(generated_noise)
        example = example.cpu().detach().numpy()
        example = example.swapaxes(1, 3).squeeze()

        axs[2,i].imshow(np.rot90(example, 3))
        test = example.reshape(img_shape)
        ch_gen = np.array(test).reshape(-1, IMAGE_SIZE, IMAGE_SIZE)
        ch_gen = pd.DataFrame(sum_channels_parallel(ch_gen)).values
        axs[2,i].set_title("Generated\n" + str(ch_gen.astype(int)[0]))
        axs[2, i].axis('off')
    plt.savefig(f"{save_dir}/images/examples.png")

    final_ch_gen = []
    final_ch_org = []
    for x, cond_x in validation_loader:
        example = autoencoder.full_generate(torch.randn(cond_x.shape[0], NOISE_INPUT_SIZE).to(device),
                                            cond_x.to(device))

        test = example.reshape(-1, IMAGE_SIZE, IMAGE_SIZE).cpu().detach().numpy()
        x = x.reshape(-1, IMAGE_SIZE, IMAGE_SIZE).cpu().detach().numpy()
        ch_gen = np.exp(np.array(test).reshape(-1, IMAGE_SIZE, IMAGE_SIZE)) - 1
        ch_gen = pd.DataFrame(sum_channels_parallel(ch_gen)).values

        ch_org = np.exp(np.array(x).reshape(-1, IMAGE_SIZE, IMAGE_SIZE)) - 1
        ch_org = pd.DataFrame(sum_channels_parallel(ch_org)).values
        final_ch_gen.append(ch_gen)
        final_ch_org.append(ch_org)
    final_ch_gen = np.concatenate(final_ch_gen)
    final_ch_org = np.concatenate(final_ch_org)

    dists = []
    for i in range(5):
        dist = wasserstein_distance(final_ch_org[i], final_ch_gen[i])
        dists.append(dist)
        print(f"Evaluation channel {i}: {dist}")

    print(f"Average wasserstein distance: {np.mean(dists)}")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='train VAE')
    parser.add_argument('--name', help='model name', required=True)
    parser.add_argument('--epochs', help='epochs to train', default=10)
    parser.add_argument('--data',
                        help='path to data dir with data_nonrandom_responses.npz and data_nonrandom_particles.npz',
                        default="data")
    parser.add_argument('--gpu_id', help='Which GPU to use, set to -1 if no GPU available', default=0)
    #### Training params
    parser.add_argument('--testset_size', help='Size of the randomly choosen testset', default=0.2)
    parser.add_argument('--batch_size', help='Size of the batch used for training', default=256)
    parser.add_argument('--lr', help='Learning rate of the optimizer used for training', default=0.0001)
    args = parser.parse_args()

    main(args)
