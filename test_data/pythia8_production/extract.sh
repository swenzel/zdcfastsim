for i in `seq 0 111`; do
  file="o2sim${i}.root"
  if [ -f "$file" ]; then
    C="../zdcfastsim/data_preparation/Extract.C(\"${file}\")"
    root -q -b -l ${C}
  
    mv zero_examples.txt zero_examples_run${i}.txt
    mv non_zero_examples.txt non_zero_examples_run${i}.txt
    mv proton_image.txt proton_image_run${i}.txt
    mv neutron_image.txt neutron_image_run${i}.txt
  fi
done
