for i in `seq 57 111`; do
  echo ${i}
  o2-sim-serial -n 500 -g pythia8 --seed ${i} -o o2sim${i} --configFile configuration_in.ini > log${i} 2>&1 &
done
