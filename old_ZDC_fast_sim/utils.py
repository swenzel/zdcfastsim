import matplotlib.pyplot as plt
import pandas as pd

def generate_image(data, outpath):
    plt.imshow(data)
    plt.savefig(outpath)


def generate_comparison(truth, prediciton, cond_data,  outpath):
    cond_data = pd.DataFrame(cond_data, columns = ['Energy','Vx' , 'Vy', 'Vz', 'mass', 'charge'])
    fig, (ax1, ax2) = plt.subplots(1, 2)
    cond_info_str = str(cond_data.to_dict('records')).replace(",","\n")
    plt.suptitle(cond_info_str, fontsize='small', ha='left', x=0.33)
    ax1.imshow(truth)
    ax1.set_title('truth')
    ax2.imshow(prediciton)
    ax2.set_title('prediction')
    plt.savefig(outpath)
