import numpy as np
import os
import tensorflow as tf
from tensorflow.python.keras import backend as K, optimizers
from tensorflow.python.keras import metrics
from tensorflow.python.keras.layers import Input, Dense, Lambda, Reshape, Conv2DTranspose, Conv2D, LeakyReLU, Flatten, Dropout, merge
from tensorflow.python.keras.models import Model, save_model
import pandas as pd
from utils import generate_comparison
from sklearn.preprocessing import MinMaxScaler

K.clear_session()

os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   
os.environ["CUDA_VISIBLE_DEVICES"]="0"


data = np.loadtxt("./data/neutron_image.txt")
data = np.reshape(data, (-1, 44, 44))
data = np.reshape(data, (-1, 44 * 44))

scaler = MinMaxScaler()
cond_data = pd.read_csv("./data/non_zero_examples_with_particles.txt", sep=' ')
# only neutrons
cond_data = cond_data[cond_data['Detector'] == 'N']
cond_data = cond_data[['Energy','Vx' , 'Vy', 'Vz', 'mass', 'charge']]
cond_data = pd.DataFrame(scaler.fit_transform(cond_data), columns=cond_data.columns)
cond_data = cond_data.to_numpy()



def trainVAE(epochs=50):
    batch_size = 16
    original_dim = 44*44
    cond_dim = cond_data.shape[1]  # len(cond_data[0])
    latent_dim = 100
    intermediate_dim = 200
    epsilon_std = 1.0

    # image = (44, 44, 1)

    x = Input(shape=(original_dim,))
    x_reshaped = Reshape((44, 44, 1))(x)
    x_conv = Conv2D(30,(3,3),padding='same')(x_reshaped)
    x_conv2 = Conv2D(20, (3, 3), padding='same')(x_conv)
    x_conv3 = Conv2D(10, (3, 3), padding='same')(x_conv2)

    cond = Input(shape=(cond_dim,))
    x_flatten =Flatten()(x_conv3)
    x_with_cond = merge.concatenate([x_flatten, cond], axis=1)

    h = Dense(intermediate_dim, activation='relu')(x_with_cond)
    z_mean = Dense(latent_dim)(h)
    z_log_sigma = Dense(latent_dim)(h)

    def sampling(args):
        z_mean, z_log_var = args
        epsilon = K.random_normal(shape=(K.shape(z_mean)[0], latent_dim), mean=0.,
                                  stddev=epsilon_std)
        return z_mean + K.exp(z_log_var / 2) * epsilon


    z = Lambda(sampling, output_shape=(latent_dim,))([z_mean, z_log_sigma])
    z_cond = merge.concatenate([z, cond], axis=1)

    decoder_h = Dense(intermediate_dim, activation='relu')
    decoder_mean = Dense(original_dim, activation='sigmoid')

    conv1 = Conv2DTranspose(original_dim // 2, (3, 3), padding='same')
    conv2 = Conv2DTranspose(original_dim // 4, (3, 3), padding='same')
    conv3 = Conv2DTranspose(original_dim // 4, (3, 3), padding='same')
    outConv = Conv2DTranspose(1, 3, padding='same')

    #Generator VAE
    h_decoded = decoder_h(z_cond)
    x_decoded_mean = decoder_mean(h_decoded)
    x_decoded_mean_reshaped = Reshape((44, 44, 1))(x_decoded_mean)
    c1 = conv1(x_decoded_mean_reshaped)
    c1_a = LeakyReLU()(c1)
    c3 = conv3(c1_a)
    c3_a = LeakyReLU()(c3)
    c3_ad = Dropout(0.4)(c3_a)
    out = outConv(c3_ad)
    out_reshaped = Flatten()(out)


    # autoencoder
    vae = Model(inputs=[x, cond], outputs=x_decoded_mean)
    #plot_model(vae, to_file='./model_visualisation/condCVAE_plot.png', show_shapes=True, show_layer_names=True)
    
    #generator
    decoder_input = Input(shape=(latent_dim + cond_dim,))
    _h_decoded = decoder_h(decoder_input)
    _x_decoded_mean = decoder_mean(_h_decoded)
    _x_decoded_mean_reshaped = Reshape((44, 44, 1))(_x_decoded_mean)
    _c1 = conv1(_x_decoded_mean_reshaped)
    _c1_a = LeakyReLU()(_c1)
    _c3 = conv3(_c1_a)
    _c3_a = LeakyReLU()(_c3)
    _c3_ad = Dropout(0.4)(_c3_a)
    _out = outConv(_c3_ad)

    generator = Model(decoder_input, _out)

    def vae_loss2(y_true, y_pred):
        """ Calculate loss = reconstruction loss + KL loss for each data in minibatch """
        # E[log P(X|z)]
        recon = original_dim * metrics.mean_squared_error(x, out_reshaped)
        # D_KL(Q(z|X) || P(z|X)); calculate in closed form as both dist. are Gaussian
        kl = 0.5 * K.sum(K.exp(z_log_sigma) + K.square(z_mean) - 1. - z_log_sigma, axis=1)
        return recon + 0.7 * kl

    rms_opt = optimizers.RMSprop(lr=0.0001)
    adam_opt = optimizers.Adam(lr=0.00005, decay=3e-7)
    vae.compile(optimizer=adam_opt, loss=vae_loss2)

    x_train = np.reshape(data, (-1, original_dim))

    vae.fit([x_train, cond_data], x_train, shuffle=True,
            epochs=epochs,
            batch_size=batch_size,
            validation_split=0.3)

    save_model(generator,"./models/condCVAE_gen.h5")
    save_model(vae,"./models/condCVAE.h5")

    samples = [1, 50, 100, 150, 200, 500, 750]

    for sample in samples:
        truth = np.reshape(data[sample - 1:sample], (44, 44))
        predict = np.reshape(vae.predict([data[sample - 1:sample], cond_data[sample - 1:sample]]), (44, 44))
        generate_comparison(truth, predict, cond_data[sample -1: sample], "./plots/condCVAE/comparision_" + str(sample))


    return vae, generator


trainVAE(epochs=200)
