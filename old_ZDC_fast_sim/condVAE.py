import numpy as np
import os
import tensorflow as tf
from tensorflow.python.keras import backend as K, optimizers
from tensorflow.python.keras import metrics
from tensorflow.python.keras.layers import Input, Dense, Lambda, Reshape, Conv2DTranspose, Conv2D, LeakyReLU, Flatten, Dropout, merge
from tensorflow.python.keras.models import Model, save_model
import pandas as pd
from utils import generate_comparison
from sklearn.preprocessing import MinMaxScaler

K.clear_session()

os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   
os.environ["CUDA_VISIBLE_DEVICES"]="0"

scaler = MinMaxScaler()

data = np.loadtxt("./data/neutron_image.txt")
data = np.reshape(data, (-1, 44, 44))
data = np.reshape(data, (-1, 44 * 44))

cond_data = pd.read_csv("./data/non_zero_examples_with_particles.txt", sep=' ')
# only neutrons
cond_data = cond_data[cond_data['Detector'] == 'N']
cond_data = cond_data[['Energy','Vx' , 'Vy', 'Vz', 'mass', 'charge']]
cond_data = pd.DataFrame(scaler.fit_transform(cond_data), columns=cond_data.columns)
cond_data = cond_data.to_numpy()


def trainVAE(epochs=200):
    batch_size = 32
    original_dim = 44 * 44
    cond_dim = cond_data.shape[1]  # len(cond_data[0])
    latent_dim = 100
    intermediate_dim_2 = 200
    intermediate_dim = 500
    epsilon_std = 1.0

    # image = (44, 44, 1)

    x = Input(shape=(original_dim,))
    cond = Input(shape=(cond_dim,))
    inputs = merge.concatenate([x, cond], axis=1)

    h = Dense(intermediate_dim, activation='relu')(inputs)
    h_2 = Dense(intermediate_dim_2, activation='relu')(h)
    z_mean = Dense(latent_dim)(h_2)
    z_log_sigma = Dense(latent_dim)(h_2)

    def sampling(args):
        z_mean, z_log_var = args
        epsilon = K.random_normal(shape=(K.shape(z_mean)[0], latent_dim), mean=0.,
                                  stddev=epsilon_std)
        return z_mean + K.exp(z_log_var / 2) * epsilon

    z = Lambda(sampling, output_shape=(latent_dim,))([z_mean, z_log_sigma])
    z_cond = merge.concatenate([z, cond], axis=1)

    decoder_h_2 = Dense(intermediate_dim_2, activation='relu')
    decoder_h = Dense(intermediate_dim, activation='relu')
    decoder_mean = Dense(original_dim, activation='sigmoid')

    h_decoded_2 = decoder_h_2(z_cond)
    h_decoded = decoder_h(h_decoded_2)
    x_decoded_mean = decoder_mean(h_decoded)
    x_decoded_mean_reshaped = Reshape((44, 44, 1))(x_decoded_mean)

    # autoencoder
    vae = Model(inputs=[x, cond], outputs=x_decoded_mean)
    #plot_model(vae, to_file='./model_visualisation/condVAE_plot.png', show_shapes=True, show_layer_names=True)


    # generator

    def vae_loss(y_true, y_pred):
        """ Calculate loss = reconstruction loss + KL loss for each data in minibatch """
        # E[log P(X|z)]
        recon = original_dim * metrics.mean_squared_error(x, x_decoded_mean)
        # D_KL(Q(z|X) || P(z|X)); calculate in closed form as both dist. are Gaussian
        kl = 0.3 * K.sum(K.exp(z_log_sigma) + K.square(z_mean) - 1. - z_log_sigma, axis=1)
        return recon + kl

    rms_opt = optimizers.RMSprop(lr=0.0001)
    adam_opt = optimizers.Adam(lr=0.001)
    vae.compile(optimizer=adam_opt, loss=vae_loss)

    x_train = np.reshape(data, (-1, original_dim))

    vae.fit([x_train, cond_data], x_train,
            shuffle=True,
            epochs=epochs,
            batch_size=batch_size,
            validation_split=0.3)

    decoder_input = Input(shape=(latent_dim + cond_dim,))
    _h_decoded_2 = decoder_h_2(decoder_input)
    _h_decoded = decoder_h(_h_decoded_2)
    _x_decoded_mean = decoder_mean(_h_decoded)
    x_decoded_mean_reshaped = Reshape((44, 44, 1))(_x_decoded_mean)
    generator = Model(decoder_input, x_decoded_mean_reshaped)

    save_model(generator,"./models/vae_gen.h5")
    save_model(vae,"./models/vae.h5")

    samples =  [1, 50, 100,150,200, 500, 750]
    for sample in samples:
        truth = np.reshape(data[sample - 1:sample], (44, 44))
        predict = np.reshape(vae.predict([data[sample - 1:sample], cond_data[sample - 1:sample]]), (44, 44))
        generate_comparison(truth, predict, cond_data[sample -1: sample], "./plots/condVAE/comparision_" + str(sample))


    return generator


trainVAE(epochs=150)
